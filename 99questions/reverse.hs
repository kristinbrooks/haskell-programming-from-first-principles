reverse1' [] reversed     = reversed
reverse1' (x:xs) reversed = reverse1' xs (x:reversed)

reverse1 :: [a] -> [a]
reverse1 list = reverse1' list []
