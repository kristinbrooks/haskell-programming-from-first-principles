module Print3Kb where

-- can use either `String` or `[Char]` type signature as they are synonyms

myGreeting :: String
myGreeting = "hello" ++ " world!"

hello :: [Char]
hello = "hello"

world :: [Char]
world = "world!"

main :: IO ()
main = do
  putStrLn myGreeting
  putStrLn secondGreeting
  where secondGreeting =
          concat [hello, " ", world]
