module Print3BrokenKb where

greeting = "Yarrrr"   -- greeting moved here to put in scope of both
                      -- `printSecond` and `main`

printSecond :: IO ()
printSecond = do
  putStrLn greeting   -- :5:12:; 5 is line where error occurs, 12 is the column
                      -- error caused because `greeting` is not in scope

main :: IO ()
main = do
  putStrLn greeting
  printSecond
--  where greeting = "Yarrrr"   -- commented here to move into scope above
