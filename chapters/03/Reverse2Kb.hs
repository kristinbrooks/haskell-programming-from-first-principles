module Reverse2Kb where

rvrs :: String
rvrs = concat [end, middle, beginning]
  where x = "Curry is awesome"
        beginning = take 5 x
        middle = drop 5 (take 9 x)
        end = drop 9 x

main :: IO ()
main = print $ rvrs
