# Chapter 3 Exercises

## Scope

1. ```
   Prelude> x = 5
   Prelude> y = 7
   Prelude> z = x * y
   ```
   Is `y` in the scope for `z`?
   
    Yes
   
2. ```
   Prelude> f = 3
   Prelude> g = 6 * f + h
   ```
   Is `h` in the scope of `g`?
   
    No
    
3. code from source file:
   ```
   area d = pi * (r * r)
   r = d / 2
   ```
   Is everything we need to execute `area` in scope?
   
    ~~Yes~~ No
    
4. code from source file:
   ```
   area d = pi * (r * r)
    where r = d / 2
   ```
   Are `r` and `d` in scope for `area`?
   
    Yes
    
## Syntax Errors

Decide whether you think it will compile, then test in REPL and try to fix
 the syntax errors where they occur.

1. `++ [1, 2, 3] [4, 5, 6]`

    Won't Compile; infix operator doesn't have parenthesis
    
2. `'<3' ++ ' Haskell'`

    Won't Compile; didn't use double quotes
    
3. `concat ["<3", " Haskell"]`

    Will Compile
    
## Reading Syntax

1. a) `concat [[1,2,3], [4,5,6]]`

    Correct syntax
    
   b) `++ [1,2,3] [4,5,6]`
   
    Incorrect syntax
    
    Corrected: `(++) [1,2,3] [4,5,6]`
        
   c) `(++) "hello" " world"`
   
    Correct syntax
    
   d) `["hello" ++ " world]`
   
    Incorrect syntax
    
    Corrected: `["hello" ++ " world"]`
    
   e) `4 !! "hello"`
   
    Incorrect syntax
    
    Corrected: `"hello" !! 4`
    
   f) `(!!) "hello" 4`
   
    Correct syntax
    
   g) `take "4 lovely"`
   
    Incorrect syntax
    
    Corrected: `take 4 "lovely"`
    
   h) `take 3 "awesome"`
   
    Correct syntax
    
2. Match code and results:

    a) `concat [[1 * 6], [2 * 6], [3 * 6]]`
    
      matches: d) `[6, 12, 18]`
        
    b) `"rain" ++ drop 2 "elbow"`
    
      matches: c) `"rainbow"`
        
    c) `10 * head [1, 2, 3]`
    
      matches: e) `10`
        
    d) `(take 3 "Julie) ++ (tail "yes"`
    
      matches: a) `"Jules"`
        
    e)
    ```
    concat [tail [1, 2, 3],
            tail [4, 5, 6],
            tail [7, 8, 9]]
    ```
      matches: b) `[2, 3, 5, 6, 8, 9]`
      
## Building Functions

1. Write a function that takes the 'given' and gives back the 'return'.

    a) given: `"Curry is awesome"`
       return: `"Curry is awesome!"`
       
      `"Curry is awesome" ++ "!"`
      
    b) given: `"Curry is awesome!"`
       return: `"y"`
       
      ~~take 4 "Curry is awesome!"~~ 
      
      ~~"Curry is awesome!" !! 4 -- returned single quotes instead of double
      ...is that ok? or do I have the wrong solution? --- wrong solution~~
      
      `take 1 $ drop 4 "Curry is awesome!"`
      
    c) given: `"Curry is awesome!"`
       return: `"awesome!"`
       
      ~~take 8 "Curry is awesome!"~~
      
      `drop 9 "Curry is awesome!"`
      
2. In Exercises03Kb.hs

3. In Exercises03Kb.hs

4. In Exercises03Kb.hs

5. In Exercises03Kb.hs

6. This is ReverseKb.hs
   
   version 2 in Reverse2Kb.hs
   