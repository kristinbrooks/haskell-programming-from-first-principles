module TopOrLocalKb where

topLevelFunction :: Integer -> Integer
topLevelFunction x =
  x + woot + topLevelValue
  where woot :: Integer   -- not necessary with type inference
        woot = 10

topLevelValue :: Integer
topLevelValue = 5
