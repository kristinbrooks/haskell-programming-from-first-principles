module Exercises03Kb where

-- ## Scope

-- problem 3
-- area d = pi * (r * r)
-- r = d / 2

-- problem 4
area' d = pi * (r * r)
    where r = d / 2

-- ## Building Functions

-- problem 2
-- a)
addBang :: String -> String
addBang s = s ++ "!"

-- b)
getIndexFour :: String -> Char
getIndexFour s = s !! 4

-- c)
dropNine :: String -> String
dropNine s = drop 9 s

-- problem 3
thirdLetter :: String -> Char
thirdLetter x = x !! 3

-- problem 4
letterIndex :: Int -> Char
letterIndex x = givenStr !! x
  where givenStr = "Curry is awesome!"

-- problem 5
rvrs = concat [drop 9 givenStr,drop 5 (take 9 givenStr), take 5 givenStr]
  where givenStr = "Curry is awesome"
