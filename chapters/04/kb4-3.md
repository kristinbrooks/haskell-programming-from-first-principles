# Exercises

## Mood swing

given: `data Mood = Blah | Woot deriving Show`

1. type constructor is `Mood`
2. possible `Mood` values are `Blah` and `Woot`
3. trying to write a `changeMood` function that acts like `not` (given one value it returns the
 other value of the same type) --- what is wrong with type signature: `changeMood :: Mood -> Woot`?

    it needs a type as the return, not a value
4. ```
   changeMood Blah = Woot
   changeMood    _ = Blah 
   ```   
5. wrote datatype and function into Kb4-3.hs and ran in GHCi --- works  
