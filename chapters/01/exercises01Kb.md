# Chapter 1 Exercises

## Equivalence Exercises, p.19

1. `𝛌xy.xz` ≈

    b) `𝛌mn.mz`
    
2. `𝛌xy.xxy` ≈

    c) `𝛌a.(𝛌b.aab)`
    
3. `𝛌xyz.zx` ≈

    b)𝛌tos.st
    
## Combinators, p.25

1. `𝛌x.xxx`

    yes, is a combinator
    
2. `𝛌xy.zx`

    no, not a combinator

3. `𝛌xyz.xy(zx)`

    yes, is a combinator

4. `𝛌xyz.xy(zxy)`

    yes, is a combinator
    
5. `𝛌xy.xy(zxy)`

    no, not a combinator
    
## Normal form or diverge?, p.25

1. `𝛌x.xxx`

    normal form, doesn't diverge
    
2. ```
   (𝛌z.zz)(𝛌y.yy)
   ([z := (𝛌y.yy)]zz)
   (𝛌y.yy)(𝛌y.yy)    
   ([y := (𝛌y.yy)]yy)
   (𝛌y.yy)(𝛌y.yy)    
   ```
   diverges
   
3. ```
   (𝛌x.xxx)z
   zzz
   ```
   
   normal form, doesn't diverge
   
## Beta Reduce

1. ```
   (𝛌abc.cba)zz(𝛌wv.w)
   (𝛌wv.w)zz
   (𝛌v.z)z
   z
   ```
   
2. ```
   (𝛌x.𝛌y.xyy)(𝛌a.a)b
   (𝛌a.a)bb
   bb
   ```
    
3. ```
   (𝛌y.y)(𝛌x.xx)(𝛌z.zq)    
   (𝛌x.xx)(𝛌z.zq)
   (𝛌z.zq)(𝛌z.zq)
   (𝛌z.zq)q    
   qq
   ```
    
4. ```
   (𝛌z.z)(𝛌z.zz)(𝛌z.zy)
   (𝛌z.zz)(𝛌z.zy)
   (𝛌z.zy)(𝛌z.zy)
   (𝛌z.zy)y
   yy
   ```
   
5. ```
   (𝛌x.𝛌y.xyy)(𝛌y.y)y
   (𝛌y.y)yy
   yy
   ```
   
6. ```
   (𝛌a.aa)(𝛌b.ba)c
   (𝛌b.ba)(𝛌b.ba)c      
   (𝛌b.ba)ac
   aac
   ```
   
7. ```
   (𝛌xyz.xz(yz))(𝛌x.z)(𝛌x.a)    # rewrite as (𝛌x.𝛌y.𝛌z1.xz1(yz1))(𝛌x.z)(𝛌x.a)
                                # (𝛌y.𝛌z1.(𝛌x.z)z1(yz1))(𝛌x.a)
   𝛌z.(𝛌x.z)z((𝛌x.a)z)          # (𝛌z1.(𝛌x.z)(z1)((𝛌x.a)z1))
   ~~(𝛌x.z)((𝛌x.a)z)~~          # (𝛌z1.z((𝛌x.a)(z1)))
   ~~z~~                        # 𝛌z1.za
   ```
   