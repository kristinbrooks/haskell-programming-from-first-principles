# Chapter 2 Exercises

## Comprehension Check

1. play with `half x = x / 2` and `square x = x * x` in REPL

2. write function that works with the following:
    ```
   3.14 * (5 * 5)
   3.14 * (10 * 10)
   3.14 * (2 * 2)
   3.14 * (4 * 4)
    ```
   
   `piTimesSquare x = 3.14 * (x * x)`
   
3. rewrite problem 2 using built in `pi` instead of 3.14

   `piTimesSquare x = pi * (x * x)`

## Parenthesis and association

1. a) `8 + 7 * 9`
    
   b) `(8 + 7) * 9`
   
   The parenthesis do chane the results because of precedence. a) is 71 vs b
   ) being 135
   
2. a) `perimeter x y = (x * 2) + (y * 2)`

   b) `perimeter x y = x * 2 + y * 2`
   
   a) and b) are the same because the parenthesis match up with their
    natural precedence, so they didn't change anything.
    
## Heal the Sick (debugging)

1. `area x = 3. 14 * (x * x)`
   
   corrected: `area x = 3.14 * (x * x)`

2. `double x = x * 2`

   corrected: `double x = x * 2`
   
3. ```
   x = 7
    y = 10
   f = x + y
   ```
   corrected:
   ```
   x = 7
   y = 10
   f = x + y
   ```

## A head code

Enter the following in the REPL to see if they evaluate to what you expect:

1. `let x = 5 in x`

    expect: 5
    correct: yes
    
2. `let x = 5 in x * x`

    expect: 25
    correct: yes
    
3. `let x = 5; y = 6 in x * y`

    expect: 30
    correct: yes
    
4. `let x = 3; y = 1000 in x + 3`

    expect: 6
    correct: yes
    
Now write the same into a file but using `where` expressions.

## Parenthesization

1. `2 + 2 * 3 - 1` = `2 + (2 * 3) - 1` = `7`

2. `(^) 10 $ 1 + 1` = `10 ^ (1 + 1)` = `100`

3. `2 ^ 2 * 4 ^ 5 + 1` = `(2 ^ 2) * (4 ^ 5) + 1` = `4097`

## Equivalent expressions

1. `1 + 1` & `2`

    same result (2)
    
2. `10 ^ 2` & `10 + 9 * 10`

    same result (100)
    
3. `400 - 37` & `(-) 37 400`

    different results; 363, -363
    
4. `100 'div' 3` & `100 / 3`

    different results; 33, 33.33...
    
5. `2 * 5 + 18` & `2 * (5 + 18)`

    different results; 28, 46
    
## More fun with functions

1. Rewrite the given source code so that it could be evaluated in the REPL:

    given:
    ```
    z = 7
    x = y ^ 2
    waxOn = x * 5
    y = z + 8
    ```
    rewritten:
    ```
    z = 7
    y = z + 8
    x = y ^ 2
    waxOn = x * 5
    ```
   what happens when enter these:
   
   `10 + waxOn` = `1135`

   `(+10) waxOn` = `1135`
    
   `(-) 15 waxOn` = `-1110`
    
   `(-) waxOn 15` = `1110`

2. Enter `triple x = x * 3` in REPL 
   
3. `triple waxOn` = `3375` 
    
    The `x` in `triple` is not the same `x` as in `waxOn`. The result of `waxOn
    ` is the argument for `triple`.
    
4.  Rewrite `waxOn` as expression with `where` clause in source file. Load
 and make sure it still works. (It does.)
 
5. Add `triple` to source file and see if `triple waxOn` still works. (It does.)

6. Add `waxOff x = triple x` to file.

7. Load and enter `waxOff waxOn` at prompt.

    It will return the same result as `triple waxOn`.
    
    `waxOff 10` = `30`
    
    `waxOff (-50)` = `-150`
