module LearnKb where

x = 10 * 5 + y

myResult = x * 5

y = 10

-- indentation matters
let x = 3
    y = 4

    -- or

let
  x = 3
  y = 4

foo x =
  let y = x * 2
      z = x ^ 2
  in 2 * y * z
